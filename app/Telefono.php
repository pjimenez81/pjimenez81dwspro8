<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');

class Telefono {

//Propiedades
    private $id;
    private $Numero;
    private $idpersona;

//Constructores


    public function __construct($id, $numero, $idpersona) {
        $this->id = $id;
        $this->numero = $numero;
        $this->idpersona = $idpersona;
    }

//Metodos
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getNumero() {
        return $this->numero;
    }

    public function setNumero($numero) {
        return $this->numero;
    }

    public function getPersona() {
        return $this->idpersona;
    }

    public function setPersona($idpersona) {
        return $this->idpersona;
    }

}

?>
