<?php

  class InstalarBD {
      
      
      protected $pdo;
	
	public function __construct()
	{
		$this->pdo = new PDO("mysql:host=localhost;", "alumno", "alumno");
	}
	//creamos la base de datos y las tablas que necesitemos
	public function instalar()
	{
        //creamos la base de datos si no existe
		$crear_db = $this->pdo->prepare('CREATE DATABASE IF NOT EXISTS ceedcv COLLATE utf8_general_ci');							  
		$crear_db->execute();
		
		//decimos que queremos usar la tabla que acabamos de crear
		if($crear_db):
		$use_db = $this->pdo->prepare('USE ceedcv');						  
		$use_db->execute();
		endif;
		
		//si se ha creado la base de datos y estamos en uso de ella creamos las tablas
		if($use_db):
		//creamos la tabla usuarios
		$crear_tb_users = $this->pdo->prepare('
						CREATE TABLE IF NOT EXISTS personas (
						id int(11) NOT NULL AUTO_INCREMENT,
						nombre varchar(100) COLLATE utf8_general_ci NOT NULL,
						PRIMARY KEY (id)
					    )');							
		$crear_tb_users->execute();
		
		//creamos la tabla posts
		$crear_tb_posts = $this->pdo->prepare('
						CREATE TABLE IF NOT EXISTS telefonos (
						id int(11) NOT NULL AUTO_INCREMENT,
                                                numero int (12) NOT NULL,
						id_persona int(11) NOT NULL,
						PRIMARY KEY (id)
						)');							
		$crear_tb_posts->execute();
                
                //creamos la tabla usuarios
                
                $crear_tb_posts = $this->pdo->prepare('
						CREATE TABLE IF NOT EXISTS usuarios (
						usuario varchar(20) NOT NULL,
                                                password varchar(20) NOT NULL,
						PRIMARY KEY (usuario)
						)');							
		$crear_tb_posts->execute();
                
                //Creamos el usuario de login alumno, con contraseña alumno.
                $crear_tb_posts = $this->pdo->prepare('
                                                INSERT INTO usuarios (usuario, password)
                                                VALUES ("alumno", "alumno")');
                
                 $crear_tb_posts->execute();
		endif;
		
	}
}

$db = new InstalarBD();
$db->instalar();

echo "<h1>Agenda de Teléfonos</h1>";
echo "<hr>";
echo "<p>CREADA BBDD: CEEDCV.</p>";
echo "<p>CREADA TABLA: PERSONAS.</p>";
echo "<p>CREADA TABLA: TELÉFONOS.</p>";
echo "<p>CREADA TABLA: USUARIOS.</p>";
echo "<p>CREADA Caj: personas.id --> teléfonos.id_persona</p>";
echo "<a href='..\login.php'>Volver al menú.</a>";


