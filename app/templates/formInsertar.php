<?php ob_start() ?>
<?php
if (isset($_SESSION['usuario'])) {
    ?>
    <?php if (isset($params['mensaje'])) : ?>
        <b><span style="color: red;"><?php echo $params['mensaje'] ?></span></b>
        <?php endif; ?>
    <br/>
    <form name="formInsertar" action="index.php?ctl=insertar" method="POST">
        <table>
            <tr>
                <th>Nombre</th>
            </tr>

            <tr>
                <td><input type="text" name="nombre" value="<?php echo $params['nombre'] ?>" /></td>
            </tr>

        </table>
        <input type="submit" value="insertar" name="insertar" />
    </form>


    <?php $contenido = ob_get_clean() ?>
    <?php include 'layout.php' ?>
    <?php
} else {
    include 'layout.php';
    $contenido = ob_get_clean();
    echo "Usted no está registrado en la aplicación.<br>";
}
?>

