<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');

class Controller {

    public function inicio() {
        $params = array(
            'mensaje' => 'Bienvenido a la agenda.',
            'fecha' => date('d-m-y'),
        );
        require __DIR__ . '/templates/inicio.php';
    }

    public function listar() {


//Al crear el objeto, conectamos con la BD con los parámetros de config.php
        $m = new ModeloMysql(Config::$mvc_bd_nombre, Config::$mvc_bd_usuario, Config::$mvc_bd_clave, Config::$mvc_bd_hostname);

//Llamamos al método dameAlimentos del modelo y cargaremos los resultados en el array $params
        $params = array('personas' => $m->readPersona(),);

        require __DIR__ . '/templates/mostrarPersonas.php';
    }

    public function listarTelefonos() {


//Al crear el objeto, conectamos con la BD con los parámetros de config.php
        $m = new ModeloMysql(Config::$mvc_bd_nombre, Config::$mvc_bd_usuario, Config::$mvc_bd_clave, Config::$mvc_bd_hostname);

//Llamamos al método dameAlimentos del modelo y cargaremos los resultados en el array $params
        $params = array('telefonos' => $m->readTelefono(),);

        require __DIR__ . '/templates/mostrarTelefonos.php';
    }

    public function insertar() {
        $params = array(
            'nombre' => '',
        );

        $m = new ModeloMysql(Config::$mvc_bd_nombre, Config::$mvc_bd_usuario, Config::$mvc_bd_clave, Config::$mvc_bd_hostname);

//echo "Post insert " . $_POST['insertar'];

        if (isset($_POST['insertar'])) {

// comprobar campos formulario
            if ($m->validarDatos($_POST['nombre'])) {
                if ($m->createPersona($_POST['nombre'])) {
                    header('Location: index.php?ctl=listar');
                } else {

                    $params = array(
                        'nombre' => $_POST['nombre'],
                    );
                    $params['mensaje'] = 'No se ha podido insertar la persona. Revisa el formulario';
                }
            } else {
                $params = array(
                    'nombre' => $_POST['nombre'],
                );
                $params['mensaje'] = 'Hay datos que no son correctos. Revisa el formulario';
            }
        }

        require __DIR__ . '/templates/formInsertar.php';
    }

    public function insertarTelefonos() {
        $params = array(
            'numero' => '',
        );

        $m = new ModeloMysql(Config::$mvc_bd_nombre, Config::$mvc_bd_usuario, Config::$mvc_bd_clave, Config::$mvc_bd_hostname);

//echo "Post insert " . $_POST['insertar'];

        if (isset($_POST['insertar'])) {

// comprobar campos formulario
            if ($m->validarDatos($_POST['numero'])) {
                if ($m->createTelefono($_POST['numero'], $_POST['idpersona'])) {
                    header('Location: index.php?ctl=listarTelefonos');
                } else {

                    $params = array(
                        'numero' => $_POST['numero'],
                        'idpersona' => $_POST['idpersona']
                    );
                    $params['mensaje'] = 'No se ha podido insertar el número. Revisa el formulario';
                }
            } else {
                $params = array(
                    'nombre' => $_POST['numero'],
                );
                $params['mensaje'] = 'Hay datos que no son correctos. Revisa el formulario';
            }
        }

        require __DIR__ . '/templates/formInsertarTelefono.php';
    }

    public function buscarPorPersona() {
        $params = array(
            'nombre' => '',
            'resultado' => array(),
        );

        $m = new ModeloMysql(Config::$mvc_bd_nombre, Config::$mvc_bd_usuario, Config::$mvc_bd_clave, Config::$mvc_bd_hostname);

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $params['nombre'] = $_POST['nombre'];
            $params['resultado'] = $m->searchPersona($_POST['nombre']);
        }

        require __DIR__ . '/templates/buscarPorNombre.php';
    }

    public function buscarTelefonos() {
        $params = array(
            'numero' => '',
            'resultado' => array(),
        );

        $m = new ModeloMysql(Config::$mvc_bd_nombre, Config::$mvc_bd_usuario, Config::$mvc_bd_clave, Config::$mvc_bd_hostname);

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $params['numero'] = $_POST['numero'];
            $params['resultado'] = $m->searchTelefono($_POST['numero'], $_POST['idpersona']);
        }

        require __DIR__ . '/templates/buscarPorTelefono.php';
    }

    public function ver() {
        if (!isset($_GET['id'])) {
            $params = array(
                'mensaje' => 'No has seleccionado ningun elemento que mostrar',
                'fecha' => date('d-m-y'),
            );
            require __DIR__ . '/templates/inicio.php';
        }

        $id = $_GET['id'];

        $m = new ModeloMysql(Config::$mvc_bd_nombre, Config::$mvc_bd_usuario, Config::$mvc_bd_clave, Config::$mvc_bd_hostname);

        $alimento = $m->searchPersona($id);

        $params = $alimento;
//Si la consulta no ha devuelto resultados volvemos a la página de inicio
        if (empty($params)) {
            $params = array(
                'mensaje' => 'No hay Persona que mostar',
                'fecha' => date('d-m-y'),
            );
            require __DIR__ . '/templates/inicio.php';
        } else
            require __DIR__ . '/templates/mostrarPersonas.php';
    }

    public function verTelefonos() {
        if (!isset($_GET['id'])) {
            $params = array(
                'mensaje' => 'No has seleccionado ningun elemento que mostrar',
                'fecha' => date('d-m-y'),
            );
            require __DIR__ . '/templates/inicio.php';
        }

        $id = $_GET['id'];

        $m = new ModeloMysql(Config::$mvc_bd_nombre, Config::$mvc_bd_usuario, Config::$mvc_bd_clave, Config::$mvc_bd_hostname);

        $alimento = $m->searchTelefono($id);

        $params = $alimento;
//Si la consulta no ha devuelto resultados volvemos a la página de inicio
        if (empty($params)) {
            $params = array(
                'mensaje' => 'No hay Telefonos que mostar',
                'fecha' => date('d-m-y'),
            );
            require __DIR__ . '/templates/inicio.php';
        } else
            require __DIR__ . '/templates/mostrarTelefonos.php';
    }

    function login() {
        $params = array(
            'mensaje' => 'Bienvenido a la agenda.',
            'fecha' => date('d-m-y'),
        );
        $usuario = recoge("usuario");
        $password = recoge("password");
        if ($usuario === "alumno" && $password === "alumno") {
            $_SESSION["autenticar"] = "autenticado";

            require __DIR__ . '/templates/inicio.php';
            /* if (!isset($_SESSION['modelo'])) {
              require __DIR__ . '/../templates/InstalarBD.php';
              } else {
              require __DIR__ . '/templates/inicio.php';
              } */
        } else {
            require __DIR__ . '/templates/login.php';
        }



        /* $usuario = recoge("usuario");
          $password = recoge("password");
          if (isset($_POST['usuario']) && !empty($_POST['usuario']) && isset($_POST['password']) && !empty($_POST['password'])) {


          $pdo = new PDO('mysql:host=localhost;dbname=ceedcv', 'alumno', 'alumno');
          $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

          $login = $pdo->prepare("SELECT usuario,password FROM usuarios WHERE usuario= '$_POST[usuario]'");
          $login->execute();
          if ($login = $login->fetch(PDO::FETCH_ASSOC)):
          $_SESSION['autenticar'] = $_POST['usuario'];
          header('Location: index.php');
          else:
          echo 'Datos incorrectos';
          endif;
          } */
    }

    function logout() {
        unset($_SESSION["autenticar"]);
        //unset($_SESSION["modelo"]);
        session_destroy();

        require __DIR__ . '/../templates/logout.php';
    }

}

?>
