<?php

function recoge($valor) {
    if (isset($_REQUEST[$valor])) {
        $campo = htmlspecialchars(trim(strip_tags($_REQUEST[$valor])));
    } else {
        $campo = "";
    };
    return $campo;
}

function combobox() {


    try {

        $this->pdo = new PDO('mysql:host=localhost;dbname=ceedcv', 'alumno', 'alumno');
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } catch (Exception $e) {

        die($e->getMessage());
    };
    $result = array();
    $stm = $this->pdo->prepare("SELECT id,nombre FROM personas");
    $stm->execute();

    $result = $stm->fetchAll();

    foreach ($result as $r) {
        //echo "<option value='" . $r['id'] . "'>" . $r['nombre'] . "</option>";
        echo $r['id'];
    }

    /* $consulta = "select id,nombre from personas order by nombre asc";
      //echo $consulta;
      $result = $this->conexion->query($consulta);
      $persona = array();
      $cont = 0;

      $filas = $result->fetchAll(PDO::FETCH_OBJ);
      //print_r($filas);
      foreach ($filas as $fila) {
      echo "<option value='" . $r['id'] . "'>" . $r['nombre'] . "</option>";
      } */
}

function acceso() {
    if ($_SESSION['autenticar'] !== "autenticado") {
        header("Location: ../index.php");
    }
}

function botonDesloguear() {
    echo " <div align='left'><form action='../controlador/ControladorLogOut.php' method='post' >
	    <input type='submit' name='desloguear' value ='Cerrar sesión'/>
	</form></div>";
}

?>