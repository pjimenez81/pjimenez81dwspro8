<?php

interface Model {

    public function createPersona($persona);

    public function readPersona();

    public function searchPersona($id);

    public function createTelefono($telefono, $id_persona);

    public function readTelefono();

    public function searchTelefono($id);
}
