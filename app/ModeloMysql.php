<?php

//app/Model.php
include_once("Persona.php");
include_once("Telefono.php");

class ModeloMysql implements Model {

    protected $conexion;

    public function __construct($dbname, $dbuser, $dbpass, $dbhost) {
        $this->conexion = NULL;
        try {
            $bdconexion = new PDO('mysql:host=' . $dbhost . ';dbname='
                    . $dbname . ';charset=utf8', $dbuser, $dbpass);
            $this->conexion = $bdconexion;
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }

    /* public function dameAlimentos() {


      $consulta = "select * from personas order by nombre desc";
      //echo $consulta;
      $result = $this->conexion->query($consulta);
      $alimentos = array();
      $cont = 0;

      $filas = $result->fetchAll(PDO::FETCH_OBJ);
      //print_r($filas);
      foreach ($filas as $fila) {
      //echo $fila->id . "<br>";
      $alimento = new Persona($fila->id, $fila->nombre);
      $alimentos [$cont] = $alimento;
      $cont++;
      }

      $conexion = false;
      return $alimentos;
      }

      public function buscarAlimentosPorNombre($nombre) {


      $nombre = htmlspecialchars($nombre);

      $sql = "select * from personas where nombre like '" . $nombre .
      "' order by energia desc";
      $result = $this->conexion->query($sql);
      $alimentos = array();
      $cont = 0;

      $filas = $result->fetchAll(PDO::FETCH_OBJ);
      //print_r($filas);
      foreach ($filas as $fila) {
      //echo $fila->id . "<br>";
      $alimento = new Alimento($fila->id, $fila->nombre);
      $alimentos [$cont] = $alimento;
      $cont++;
      }

      $conexion = false;
      return $alimentos;
      }

      public function dameAlimento($id) {

      $consulta = 'SELECT * FROM personas where id=:id;';
      //$consulta = 'SELECT * FROM profesor where id="' . $profesor_->getId() . '";';
      //$result = $this->conexion->query($consulta);

      $result = $this->conexion->prepare($consulta);
      $result->execute(array(":id" => $id));

      $profesores = array();
      $cont = 0;

      $fila = $result->fetch(PDO::FETCH_OBJ);
      $alimento = new Persona($fila->id, $fila->nombre);
      $conexion = false;

      return $alimento;
      }

      public function insertarAlimento($n) {


      try {
      $sql = "insert into personas (nombre
      ) values ('" . $n . "')";

      $result = $this->conexion->prepare($sql);

      if (!$result) {
      //echo "\nPDO::errorInfo():<br>";
      //print_r($this->conexion->errorInfo());
      }

      $count = $result->execute(array(
      ":n" => $n
      ));

      //echo $consulta . "<br\>";
      //echo "Count: " . $count . "<br\>";
      //print_r($result);
      } catch (PDOException $e) {
      echo 'ERROR: ' . $e->getMessage();
      return false;
      }

      $conexion = false;
      if ($count == 1) {
      return true;
      } else {
      return false;
      }
      } */

    public function validarDatos($n) {


        $valido = is_string($n);



        return ($valido);
    }

    public function createPersona($persona) {


        /* $pdo = $this->conectar();
          $consulta = "INSERT INTO personas (nombre) VALUES (:nombre);";
          $pdo->prepare($consulta)->execute(array(":nombre" => $persona->__GET('nombre')
          ));
          $pdo = $this->desconectar(); */





        try {
            $sql = "insert into personas (nombre
          ) values ('" . $persona . "')";

            $result = $this->conexion->prepare($sql);

            if (!$result) {
                //echo "\nPDO::errorInfo():<br>";
                //print_r($this->conexion->errorInfo());
            }

            $count = $result->execute(array(
                ":n" => $n
            ));

            //echo $consulta . "<br\>";
            //echo "Count: " . $count . "<br\>";
            //print_r($result);
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            return false;
        }

        $conexion = false;
        if ($count == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function createTelefono($telefono, $id_persona) {
        try {
            $sql = "insert into telefonos (numero, idpersona
                 ) values (" . $telefono . "," . $id_persona . ")";

            $result = $this->conexion->prepare($sql);

            if (!$result) {
                //echo "\nPDO::errorInfo():<br>";
                //print_r($this->conexion->errorInfo());
            }

            $count = $result->execute(array(
                ":n" => $n
            ));

            //echo $consulta . "<br\>";
            //echo "Count: " . $count . "<br\>";
            //print_r($result);
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            return false;
        }

        $conexion = false;
        if ($count == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function readPersona() {
        $consulta = "select * from personas order by id asc";
        //echo $consulta;
        $result = $this->conexion->query($consulta);
        $persona = array();
        $cont = 0;

        $filas = $result->fetchAll(PDO::FETCH_OBJ);
        //print_r($filas);
        foreach ($filas as $fila) {
            //echo $fila->id . "<br>";
            $persona = new Persona($fila->id, $fila->nombre);
            $personas [$cont] = $persona;
            $cont++;
        }

        $conexion = false;
        return $personas;
    }

    public function readTelefono() {

        $consulta = "select * from telefonos order by id asc";
        //echo $consulta;
        $result = $this->conexion->query($consulta);
        $telefonos = array();
        $cont = 0;

        $filas = $result->fetchAll(PDO::FETCH_OBJ);
        //print_r($filas);
        foreach ($filas as $fila) {
            //echo $fila->id . "<br>";
            $telefono = new Telefono($fila->id, $fila->numero, $fila->idpersona);
            $telefonos [$cont] = $telefono;
            $cont++;
        }

        $conexion = false;
        return $telefonos;



        /* $consulta = "select * from telefonos order by id asc";
          //echo $consulta;
          $result = $this->conexion->query($consulta);
          $telefonos = array();
          //$consulta = "SELECT * FROM telefonos";
          //$pdo = $this->conexion->query($consulta);
          //$stm = $pdo->prepare($consulta);
          //$stm->execute();
          foreach ($result->fetchAll(PDO::FETCH_OBJ) as $row) {
          $telefono = new Telefono(0, "", "");
          $telefono->__SET('id', $row->id);
          $telefono->__SET('numero', $row->numero);
          $telefono->__SET('idpersona', $row->id_persona);

          $telefonos[] = $telefono;
          }
          $pdo = false;
          return $telefono; */
    }

    public function searchPersona($id) {
        $consulta = 'SELECT * FROM personas where nombre=:nombre;';
        //$consulta = 'SELECT * FROM profesor where id="' . $profesor_->getId() . '";';
        //$result = $this->conexion->query($consulta);

        $result = $this->conexion->prepare($consulta);
        $result->execute(array(":nombre" => $id));

        $personas = array();
        $cont = 0;

        $fila = $result->fetch(PDO::FETCH_OBJ);
        $persona = new Persona($fila->id, $fila->nombre);
        $conexion = false;

        return $persona;
    }

    public function searchTelefono($id) {
        $consulta = 'SELECT * FROM telefonos where numero=:numero;';
        //$consulta = 'SELECT * FROM profesor where id="' . $profesor_->getId() . '";';
        //$result = $this->conexion->query($consulta);

        $result = $this->conexion->prepare($consulta);
        $result->execute(array(":numero" => $id));

        $telefonos = array();
        $cont = 0;

        $fila = $result->fetch(PDO::FETCH_OBJ);
        $telefono = new Telefono($fila->id, $fila->numero, $fila->persona);
        $conexion = false;

        return $telefono;
    }

}

?>
